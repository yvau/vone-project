using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.NodeServices;
using Microsoft.AspNetCore.SpaServices.Prerendering;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using static Vone.Shared.Services.HttpRequestExtensions;

namespace Vone.Shared.Services
{
    public static class HttpRequestExtensions
    {
        public class IRequest
        {
            public string name { get; set; }
            public string path { get; set; }
            public string hash { get; set; }
            public string query { get; set; }
            public object @params { get; set; }
            public object fullpath { get; set; }
            public object meta { get; set; }
            public object from { get; set; }
        }

        public static string TransFormToJson(string queryString)
        {
            var dict = HttpUtility.ParseQueryString(queryString);

            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            using (JsonTextWriter writer = new JsonTextWriter(sw))
            {
                writer.QuoteChar = '\'';

                JsonSerializer ser = new JsonSerializer();
                ser.Serialize(writer, dict.Cast<string>().ToDictionary(k => k, v => dict[v]));
            }

            string json = JsonConvert.SerializeObject(dict.Cast<string>().ToDictionary(k => k, v => dict[v]));

            return sb.ToString();
        }

        public static IRequest AbstractRequestInfo(this HttpRequest request) => new IRequest()
        {
            name = "request.Cookies",
            path = request.Path,
            hash = "",
            query = TransFormToJson(request.QueryString.ToString()),
            @params = request.Host,
            fullpath = request.Path + request.QueryString,
            meta = new { },
            from = new { }

        };

        [Obsolete]
        public static async Task<RenderToStringResult> BuildPrerender(this HttpRequest request) =>
            // Prerender / Serialize application (with Universal)
            await Prerenderer.RenderToString(
                "/",
                request.HttpContext.RequestServices.GetRequiredService<INodeServices>(),
                new System.Threading.CancellationTokenSource().Token,
                new JavaScriptModuleExport(request.HttpContext.RequestServices.GetRequiredService<IHostingEnvironment>().ContentRootPath + "/ClientApp/renderOnServer"),
                $"{request.Scheme}://{request.Host}{request.HttpContext.Features.Get<IHttpRequestFeature>().RawTarget}",
                request.HttpContext.Features.Get<IHttpRequestFeature>().RawTarget,
                // ** TransferData concept **
                // Here we can pass any Custom Data we want !
                // By default we're passing down Cookies, Headers, Host from the Request object here
                new TransferData
                {
                    auth = new { authenticated = request.HttpContext.User.Identity.IsAuthenticated },
                    alert = new { status = false, success = "null", message = "null" },
                    account = new { },
                    // account = new { email = request.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value},
                    // account = new { email = request.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value, firstName = request.HttpContext.User.FindFirst(ClaimTypes.Surname).Value, lastName = request.HttpContext.User.FindFirst(ClaimTypes.Name).Value, photoUrl = request.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value },
                    route = request.AbstractRequestInfo()
                }, // Our simplified Request object & any other CustommData you want to send!
                30000,
                request.PathBase.ToString()
            );
    }

    internal class TransferData
    {
        public Object auth { get; set; }
        public Object alert { get; set; }
        public Object account { get; set; }
        public IRequest route { get; set; }
        // public Object {"name":"account.setting.information","path":"/account/setting/information/","hash":"","query":{},"params":{},"fullPath":"/account/setting/information/"
    }

}
