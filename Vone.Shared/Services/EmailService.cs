﻿

using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;
using System;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Vone.Shared.Models;

namespace Vone.Shared.Services
{
    public interface IEmailService
    {

        Task SendEmailAsync(ApplicationUser profile, string subject, string htmlMessage);
    }

    public class EmailService : IEmailService
    {
        private readonly EmailSettings _emailSettings;
        private readonly IRazorViewToStringRenderer _razorViewToStringRenderer;

        public EmailService(IOptions<EmailSettings> emailSettings,
                           IRazorViewToStringRenderer razorViewToStringRenderer)
        {
            _razorViewToStringRenderer = razorViewToStringRenderer;
            _emailSettings = emailSettings.Value;
        }


        public async Task SendEmailAsync(ApplicationUser profile, string subject, string htmlMessage)
        {
            var template = subject;

            // string bodyHtml = await _razorViewToStringRenderer.RenderViewToStringAsync("/EmailTemplates/" + template + ".cshtml", profile);

            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress(_emailSettings.SenderName, _emailSettings.SenderEmail));
            emailMessage.To.Add(new MailboxAddress("", profile.Email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(TextFormat.Html) { Text = htmlMessage };

            using (var client = new SmtpClient())
            {
                // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                if (_emailSettings.IsDevelopment)
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                await client.ConnectAsync(_emailSettings.MailServer, _emailSettings.MailPort, _emailSettings.UseSsl).ConfigureAwait(false);

                // Note: only needed if the SMTP server requires authentication
                if (!_emailSettings.IsDevelopment)
                    await client.AuthenticateAsync(_emailSettings.SenderEmail, _emailSettings.Password).ConfigureAwait(false);

                await client.SendAsync(emailMessage).ConfigureAwait(false);
                await client.DisconnectAsync(true).ConfigureAwait(false);
            }
        }
    }
}
