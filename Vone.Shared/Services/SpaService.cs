﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Vone.Shared.Services
{
    public interface ISpaInterface
    {
        String SpaData(JToken prerender);
    }

    public class SpaService : ISpaInterface
    {

        public String SpaData(JToken prerender)
        {
            return "window.__INITIAL_STATE__ = " + prerender.ToString(Formatting.None);
        }
    }
}
