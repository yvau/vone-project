﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vone.Shared.BindingModels;

namespace Vone.Shared.Services
{
    public interface IExceptionInterface
    {
        ResponseMessage DisplayErrors(IList<ValidationFailure> Errors);
    }

    public class ExceptionService : IExceptionInterface
    {

        // display errors 
        public ResponseMessage DisplayErrors(IList<ValidationFailure> Errors)
        {
            // init lit of ErrorModel
            List<ErrorModel> errorModel = new List<ErrorModel>();

            // assign value to the ErrorModel for each distinct propertyname
            foreach (var failure in Errors.GroupBy(o => o.PropertyName).Select(o => o.First()))
            {
                errorModel.Add(new ErrorModel { PropertyName = failure.PropertyName, ErrorMessage = failure.ErrorMessage });
            }

            ResponseMessage responseMessage = new ResponseMessage();
            responseMessage.ContentResult = errorModel;
            responseMessage.IsSuccess = false;


            return responseMessage;
        }
    }
}
