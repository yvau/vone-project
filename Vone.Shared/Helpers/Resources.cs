﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vone.Shared.Helpers
{
    public class Resources
    {
        // DATA MODEL
        public static String BLOG = "blog";
        public static String LOCATION = "location";
        public static String LOCATIONHASPROPOSAL = "location_has_proposal";
        public static String PROFILE = "profile";
        public static String PROPERTY = "property";
        public static String PROPERTYPHOTO = "property_photo";
        public static String REQUESTFORPROPOSAL = "request_for_proposal";
        public static String RENTINGPROPOSAL = "renting_proposal";

        // STATIC VARIABLE
        public static int LIMIT_CITIES = 300;
    }
}
