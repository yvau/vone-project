﻿using System;
using System.Collections.Generic;

namespace Vone.Shared.Models
{
    public partial class BookmarkProposal
    {
        public DateTime? CreatedAt { get; set; }
        public long RequestProposalId { get; set; }
        public string AspNetUsersId { get; set; }

        public virtual ApplicationUser AspNetUsers { get; set; }
        public virtual RequestProposal RequestProposal { get; set; }
    }
}
