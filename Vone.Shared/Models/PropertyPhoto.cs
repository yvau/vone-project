﻿using System;
using System.Collections.Generic;

namespace Vone.Shared.Models
{
    public partial class PropertyPhoto
    {
        public long Id { get; set; }
        public string ContentType { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string Name { get; set; }
        public decimal? Size { get; set; }
        public string Url { get; set; }
        public long PropertyId { get; set; }

        public virtual Property Property { get; set; }
    }
}
