﻿using System;
using System.Collections.Generic;

namespace Vone.Shared.Models
{
    public partial class RequestHasResponseSatusLog
    {
        public RequestHasResponseSatusLog()
        {
            InverseRequestHasResponseSatusLogNavigation = new HashSet<RequestHasResponseSatusLog>();
        }

        public long Id { get; set; }
        public long RequestHasResponseSatusLogId { get; set; }
        public string Status { get; set; }
        public string Verb { get; set; }
        public DateTime? CreatedAt { get; set; }
        public long RequestHasResponseId { get; set; }
        public string AspNetUsersId { get; set; }
        public string AspNetUsersRecipientId { get; set; }

        public virtual ApplicationUser AspNetUsers { get; set; }
        public virtual ApplicationUser AspNetUsersRecipient { get; set; }
        public virtual RequestHasResponse RequestHasResponse { get; set; }
        public virtual RequestHasResponseSatusLog RequestHasResponseSatusLogNavigation { get; set; }
        public virtual ICollection<RequestHasResponseSatusLog> InverseRequestHasResponseSatusLogNavigation { get; set; }
    }
}
