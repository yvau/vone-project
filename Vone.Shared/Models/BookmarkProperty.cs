﻿using System;
using System.Collections.Generic;

namespace Vone.Shared.Models
{
    public partial class BookmarkProperty
    {
        public DateTime? CreatedAt { get; set; }
        public long PropertyId { get; set; }
        public string AspNetUsersId { get; set; }

        public virtual ApplicationUser AspNetUsers { get; set; }
        public virtual Property Property { get; set; }
    }
}
