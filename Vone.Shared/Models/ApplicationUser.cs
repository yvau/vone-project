﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace Vone.Shared.Models
{
    public partial class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
            Appointment = new HashSet<Appointment>();
            Blog = new HashSet<Blog>();
            BookmarkProperty = new HashSet<BookmarkProperty>();
            BookmarkProposal = new HashSet<BookmarkProposal>();
            Property = new HashSet<Property>();
            RequestHasResponseSatusLogApplicationUser = new HashSet<RequestHasResponseSatusLog>();
            RequestHasResponseSatusLogApplicationUserRecipient = new HashSet<RequestHasResponseSatusLog>();
            RequestProposal = new HashSet<RequestProposal>();
            ResponseProposal = new HashSet<ResponseProposal>();
        }

        [PersonalData]
        public string Avatar { get; set; }
        
        public string Biography { get; set; }
        
        [PersonalData]
        public DateTime DateOfBirth { get; set; }
        
        public string FacebookHandler { get; set; }
        
        [PersonalData]
        public string FirstName { get; set; }
        
        [PersonalData]
        public string Gender { get; set; }
        
        public string GoogleHandler { get; set; }
        
        [PersonalData]
        public string Language { get; set; }
        
        [PersonalData]
        public string LastName { get; set; }
        
        public DateTime UpdatedAt { get; set; }
        
        public DateTime CreatedAt { get; set; }

        public virtual ICollection<Appointment> Appointment { get; set; }
        public virtual ICollection<Blog> Blog { get; set; }
        public virtual ICollection<BookmarkProperty> BookmarkProperty { get; set; }
        public virtual ICollection<BookmarkProposal> BookmarkProposal { get; set; }
        public virtual ICollection<Property> Property { get; set; }
        public virtual ICollection<RequestHasResponseSatusLog> RequestHasResponseSatusLogApplicationUser { get; set; }
        public virtual ICollection<RequestHasResponseSatusLog> RequestHasResponseSatusLogApplicationUserRecipient { get; set; }
        public virtual ICollection<RequestProposal> RequestProposal { get; set; }
        public virtual ICollection<ResponseProposal> ResponseProposal { get; set; }
    }
}
