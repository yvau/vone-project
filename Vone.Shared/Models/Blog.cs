﻿using System;
using System.Collections.Generic;

namespace Vone.Shared.Models
{
    public partial class Blog
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string TitleSlug { get; set; }
        public string Body { get; set; }
        public byte? IsPublished { get; set; }
        public string ImagePreviewUrl { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string Tags { get; set; }
        public byte? Enabled { get; set; }
        public string AspNetUsersId { get; set; }

        public virtual ApplicationUser AspNetUsers { get; set; }
    }
}
