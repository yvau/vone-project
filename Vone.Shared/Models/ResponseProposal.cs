﻿using System;
using System.Collections.Generic;

namespace Vone.Shared.Models
{
    public partial class ResponseProposal
    {
        public ResponseProposal()
        {
            LocationHasResponse = new HashSet<LocationHasResponse>();
            RequestHasResponse = new HashSet<RequestHasResponse>();
        }

        public string TypeOfProposal { get; set; }
        public string TypeOfProperties { get; set; }
        public decimal? PriceMinimum { get; set; }
        public decimal? PriceMaximum { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public long Id { get; set; }
        public string AspNetUsersId { get; set; }

        public virtual ApplicationUser AspNetUsers { get; set; }
        public virtual ICollection<LocationHasResponse> LocationHasResponse { get; set; }
        public virtual ICollection<RequestHasResponse> RequestHasResponse { get; set; }
    }
}
