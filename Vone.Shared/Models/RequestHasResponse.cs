﻿using System;
using System.Collections.Generic;

namespace Vone.Shared.Models
{
    public partial class RequestHasResponse
    {
        public RequestHasResponse()
        {
            RequestHasResponseSatusLog = new HashSet<RequestHasResponseSatusLog>();
        }

        public long Id { get; set; }
        public long? PropertyId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? AppointmentDate { get; set; }
        public string Status { get; set; }
        public long? RequestProposalId { get; set; }
        public long? AppointmentId { get; set; }
        public long ResponseProposalId { get; set; }

        public virtual Appointment Appointment { get; set; }
        public virtual Property Property { get; set; }
        public virtual RequestProposal RequestProposal { get; set; }
        public virtual ResponseProposal ResponseProposal { get; set; }
        public virtual ICollection<RequestHasResponseSatusLog> RequestHasResponseSatusLog { get; set; }
    }
}
