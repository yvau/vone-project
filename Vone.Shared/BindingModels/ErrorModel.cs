﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vone.Shared.BindingModels
{
    public class ErrorModel
    {
        public String PropertyName { get; set; }
        public String ErrorMessage { get; set; }
    }
}
