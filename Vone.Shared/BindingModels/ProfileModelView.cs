﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vone.Shared.BindingModels
{
    public class ProfileModelView
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public DateTime DateOfCreationToken { get; set; } = DateTime.Now;
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public DateTime? DateOfCreation { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
        public string Role { get; set; }
        public bool Enabled { get; set; }
        public string IpAddress { get; set; }
        public string PhoneLandline { get; set; }
        public string Phone { get; set; }
        public bool? PhoneConfirmed { get; set; }
        public string VerifyCode { get; set; }
        public string Gender { get; set; }
        public string DateOfBirth { get; set; }
        public string AgentType { get; set; }
        public string AccreditationNumber { get; set; }
        public string Website { get; set; }
        public string Linkedin { get; set; }
        public string Facebook { get; set; }
        public string Google { get; set; }
        public string Instagram { get; set; }
        public string Twitter { get; set; }
        public string Language { get; set; }
        public string CommunicationChannel { get; set; }
        public string Biography { get; set; }
        public string RedirectUrl { get; set; }
        /*public long Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Gender { get; set; }
        public string BirthDate { get; set; }
        public string Language { get; set; }
        public string FacebookHandler { get; set; }
        public string GoogleHandler { get; set; }
        public string Bio { get; set; }
        public string Role { get; set; }
        public string Email { get; set; }
        public string TokenId { get; set; }
        public string IpAddress { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public bool? Enabled { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime TokenCreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
        public string RedirectUrl { get; set; }*/
    }
}
