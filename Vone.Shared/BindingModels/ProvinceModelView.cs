﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Vone.Shared.BindingModels
{
    public class ProvinceModelView
    {

        public string Id { get; set; }
        public string Name { get; set; }
        public string NameAscii { get; set; }
        public string CountryId { get; set; }

        // public virtual Country Country { get; set; }

        [JsonIgnore]
        public virtual ICollection<CityModelView> City { get; set; }
    }
}
