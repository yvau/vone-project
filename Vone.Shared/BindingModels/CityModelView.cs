﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vone.Shared.BindingModels
{
    public class CityModelView
    {
        public string Id { get; set; }
        public string AlternateNames { get; set; }
        public string FCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Name { get; set; }
        public string NameAscii { get; set; }
        public string Timezone { get; set; }
        public string ProvinceId { get; set; }

        public virtual ProvinceModelView Province { get; set; }
    }
}
