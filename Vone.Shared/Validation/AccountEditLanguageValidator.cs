﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using Vone.Shared.BindingModels;

namespace Vone.Shared.Validation
{
    public class AccountEditLanguageValidator : AbstractValidator<ProfileModelView>
    {
        // private readonly LocationResource _locationResource;

        public AccountEditLanguageValidator()
        {
            RuleFor(profileModelView => profileModelView.Language)
                .NotEmpty();
        }
    }
}
