﻿using FluentValidation;
using Vone.Shared.BindingModels;

namespace Vone.Shared.Validation
{
    public class AccountEditInformationValidator : AbstractValidator<ProfileModelView>
    {
        // private readonly LocationResource _locationResource;

        public AccountEditInformationValidator()
        {

            RuleFor(profileModelView => profileModelView.FirstName)
                .NotEmpty();
            RuleFor(profileModelView => profileModelView.LastName)
                .NotEmpty();
            RuleFor(profileModelView => profileModelView.Gender)
                .NotEmpty();
            RuleFor(profileModelView => profileModelView.DateOfBirth)
                .NotEmpty();
            RuleFor(profileModelView => profileModelView.Facebook)
                .NotEmpty();
            RuleFor(profileModelView => profileModelView.Google)
                .NotEmpty();
            RuleFor(profileModelView => profileModelView.Biography)
                .NotEmpty();

            /* RuleFor(profileModelView => profileModelView.FirstName)
                .MinimumLength(0)
                .When(profileModelView => !String.IsNullOrWhiteSpace(profileModelView.Password)); */
        }
    }
}