﻿using FluentValidation;
using System;
using Vone.Shared.BindingModels;
using Vone.Shared.Services;

namespace Vone.Shared.Validation
{
    public class AccountEditPasswordValidator : AbstractValidator<ProfileModelView>
    {
        // private readonly IProfileInterface _profileInterface;
        private string passwordPattern = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[+@#$%]).{6,20})";

        public AccountEditPasswordValidator()
        {
            // this._profileInterface = _profileInterface;

            RuleFor(profileModelView => profileModelView.Password)
            /*.Must((profileModelView, Password) => BeAValidPassword(Password, profileModelView.Email))
            .WithMessage("PasswordInDatabaseNotValid")
            .When(profileModelView => !String.IsNullOrWhiteSpace(profileModelView.Password))*/
            .NotEmpty();

            RuleFor(profileModelView => profileModelView.NewPassword)
            .NotEmpty()
            .Matches(passwordPattern)
            .WithMessage("PasswordRegexNotValid")
            .NotEqual(profileModelView => profileModelView.Password)
            .WithMessage("PasswordMustBeDifferentFromPrevious");

            /*RuleFor(profileModelView => profileModelView.RePassword)
            .Equal(profileModelView => profileModelView.NewPassword)
            .When(profileModelView => !String.IsNullOrWhiteSpace(profileModelView.NewPassword))
            .WithMessage("ConfirmPasswordNotMatching")
            .NotEmpty();*/
        }

        /*private bool BeAValidPassword(string Password, string Email)
        {
            var profile = _profileInterface.FindByEmail(Email);
            Boolean validPassword = BCrypt.Net.BCrypt.Verify(Password, profile.Password);
            if (validPassword)
            {
                return true;
            }

            return false;
        }*/
    }
}