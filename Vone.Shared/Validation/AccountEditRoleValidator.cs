﻿using FluentValidation;
using Vone.Shared.BindingModels;

namespace Vone.Shared.Validation
{
    public class AccountEditRoleValidator : AbstractValidator<ProfileModelView>
    {
        // private readonly LocationResource _locationResource;

        public AccountEditRoleValidator()
        {

            RuleFor(profileModelView => profileModelView.Role)
                .NotEmpty();
        }
    }
}