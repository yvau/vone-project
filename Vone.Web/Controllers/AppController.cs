﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Vone.Shared.Services;

namespace Vone.Web.Controllers
{
    [Route("/app/")]
    [Obsolete]
    [Authorize]
    public class AppController : Controller
    {

        private readonly ISpaInterface _spaInterface;
        private readonly ILogger<AppController> _logger;

        public AppController(ISpaInterface spaInterface,
                              ILogger<AppController> logger)
        {
            _spaInterface = spaInterface;
            _logger = logger;
        }

        [HttpGet("home/")]
        public async Task<IActionResult> HomeAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _spaInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [HttpGet("dashboard/")]
        public async Task<IActionResult> DashboardAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _spaInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }
    }
}
