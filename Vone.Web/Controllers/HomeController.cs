﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Vone.Shared.Services;
using Vone.Web.Models;

namespace Vone.Web.Controllers
{
    [Route("/")]
    [Obsolete]
    [AllowAnonymous]
    public class HomeController : Controller
    {
        private readonly ISpaInterface _spaInterface;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ISpaInterface spaInterface, 
                              ILogger<HomeController> logger)
        {
            _spaInterface = spaInterface;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> IndexAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _spaInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [HttpGet("about/")]
        public async Task<IActionResult> AboutAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _spaInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [HttpGet("concept/")]
        public async Task<IActionResult> ConceptAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _spaInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [HttpGet("advantage/")]
        public async Task<IActionResult> AdvantageAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _spaInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [HttpGet("relationship/")]
        public async Task<IActionResult> RelationshipAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _spaInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [HttpGet("blogs/")]
        public async Task<IActionResult> BlogsAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _spaInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [HttpGet("privacy/")]
        public async Task<IActionResult> Privacy()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _spaInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
