﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Vone.Shared.Services;

namespace Vone.Web.Controllers
{
    public class SettingController : Controller
    {
        private readonly ISpaInterface _spaInterface;
        private readonly ILogger<SettingController> _logger;

        public SettingController(ISpaInterface spaInterface,
                              ILogger<SettingController> logger)
        {
            _spaInterface = spaInterface;
            _logger = logger;
        }
        public IActionResult Index()
        {
            return View("/Views/Global/Home.cshtml");
        }
    }
}
