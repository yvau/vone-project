﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Vone.Shared.Services;

namespace Vone.Web.Controllers
{
    [Route("/blog/")]
    public class BlogController : Controller
    {
        private readonly ISpaInterface _spaInterface;
        private readonly ILogger<BlogController> _logger;

        public BlogController(ISpaInterface spaInterface,
                              ILogger<BlogController> logger)
        {
            _spaInterface = spaInterface;
            _logger = logger;
        }

        [HttpGet("{id}/")]
        public async Task<IActionResult> ShowAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _spaInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [HttpGet("new/")]
        public async Task<IActionResult> NewAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _spaInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [HttpGet("{id}/edit/")]
        public async Task<IActionResult> EditAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _spaInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [HttpGet("list/")]
        public async Task<IActionResult> ListAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _spaInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }
    }
}
