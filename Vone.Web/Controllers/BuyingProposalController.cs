﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Vone.Shared.Services;

namespace Vone.Web.Controllers
{
    [Route("/buy/")]
    [Obsolete]
    public class BuyingProposalController : Controller
    {
        private readonly ISpaInterface _spaInterface;
        private readonly ILogger<BuyingProposalController> _logger;

        public BuyingProposalController(ISpaInterface spaInterface,
                              ILogger<BuyingProposalController> logger)
        {
            _spaInterface = spaInterface;
            _logger = logger;
        }

        [HttpGet("new/")]
        // GET: /<controller>/
        public async Task<IActionResult> NewAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _spaInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        // GET: /<controller>/
        [HttpGet("list/")]
        public async Task<IActionResult> ListAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _spaInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        // GET: /<controller>/
        [HttpGet("{id}/")]
        public async Task<IActionResult> ShowAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _spaInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }

        [HttpGet("{id}/edit/")]
        public async Task<IActionResult> EditAsync()
        {
            var prerenderResult = await Request.BuildPrerender();

            ViewData["SpaHtml"] = prerenderResult.Html;
            ViewData["SpaData"] = _spaInterface.SpaData(prerenderResult.Globals["__INITIAL_STATE__"]);

            return View("/Views/Global/Home.cshtml");
        }
    }
}
