﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Vone.Shared.BindingModels;
using Vone.Shared.Data;
using Vone.Shared.Helpers;

namespace Vone.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CityController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private IMapper _mapper;

        public CityController(ApplicationDbContext context,
                                IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Cities
        /*[HttpGet]
        public async Task<IEnumerable<CityModelView>> GetCity(String name)
        {
            if (String.IsNullOrWhiteSpace(name)) 
            {
                IEnumerable<City> cityNoParams = await _context.City.Take(Resources.LIMIT_CITIES).ToListAsync();
                var cityListNoParams = _mapper.Map<IEnumerable<CityModelView>>(cityNoParams);

                return cityListNoParams;
            }
            IEnumerable<City> cityWithParams = await _context.City.Where(b => EF.Functions.Like(b.NameAscii.ToLower(), $"%{name.ToLower()}%") || EF.Functions.Like(b.Name.ToLower(), $"%{name.ToLower()}%")).Include(c => c.Province).OrderBy(b => b.Name).Take(Resources.LIMIT_CITIES).ToListAsync();
            var cityListWithParams = _mapper.Map<IEnumerable<CityModelView>>(cityWithParams);
         
            return cityListWithParams;
        }*/
    }
}
