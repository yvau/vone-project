﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Vone.Shared.Services;
using Vone.Shared.Validation;
using Vone.Shared.BindingModels;
using FluentValidation.Results;
using Microsoft.AspNetCore.Identity;
using Vone.Shared.Models;

namespace Vone.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {

        private const string AuthenticatorUriFormat = "otpauth://totp/{0}:{1}?secret={2}&issuer={0}&digits=6";
        private readonly IExceptionInterface _exceptionInterface;
        private readonly UserManager<ApplicationUser> _userManager;

        public AccountController(IExceptionInterface exceptionInterface,
                                 IHttpContextAccessor accessor,
                                 UserManager<ApplicationUser> userManager,
                                 // IProfileInterface profileInterface,
                                 UrlEncoder urlEncoder)
        {
            _exceptionInterface = exceptionInterface;
            _userManager = userManager;
            /*_accessor = accessor;
            // _profileInterface = profileInterface;
            _urlEncoder = urlEncoder;
            _context = context;*/
        }

        // POST: /<controller>/
        [HttpPost("setting/information/")]
        public async Task<IActionResult> AccountEditInformationsAsync([FromBody] ProfileModelView profileModelView)
        {
            // check validation
            AccountEditInformationValidator validator = new AccountEditInformationValidator();
            ValidationResult results = validator.Validate(profileModelView);

            // if validation is unsuccessful show error
            if (!results.IsValid)
            {
                var errors = _exceptionInterface.DisplayErrors(results.Errors);
                return Ok(errors);
            }

            var user = await _userManager.GetUserAsync(User);
            user.FirstName = profileModelView.FirstName;
            user.LastName = profileModelView.LastName;
            user.Gender = profileModelView.Gender;
            user.DateOfBirth = Convert.ToDateTime(profileModelView.DateOfBirth);
            user.FacebookHandler = profileModelView.Facebook;
            user.GoogleHandler = profileModelView.Google;
            user.Biography = profileModelView.Biography;
            user.UpdatedAt = profileModelView.UpdatedAt;

            await _userManager.UpdateAsync(user);

            return Ok(new ResponseMessage { IsSuccess = true, Message = $"your informations have been changed" });
        }

        // POST: /<controller>/
        [HttpPost("setting/password/")]
        public async Task<IActionResult> AccountEditPasswordAsync([FromBody] ProfileModelView profileModelView)
        {
            // check validation
            AccountEditPasswordValidator validator = new AccountEditPasswordValidator();
            ValidationResult results = validator.Validate(profileModelView);

            // if validation is unsuccessful show error
            if (!results.IsValid)
            {
                var errors = _exceptionInterface.DisplayErrors(results.Errors);
                return Ok(errors);
            }

            var user = await _userManager.GetUserAsync(User);

            await _userManager.ChangePasswordAsync(user, profileModelView.NewPassword, profileModelView.Password);

            return Ok(new ResponseMessage { IsSuccess = true, Message = $"your informations have been changed" });
        }

        [HttpPost("setting/language/")]
        public async Task<IActionResult> AccountEditLanguageAsync([FromBody] ProfileModelView profileModelView)
        {
            // check validation
            AccountEditLanguageValidator validator = new AccountEditLanguageValidator();
            ValidationResult results = validator.Validate(profileModelView);

            // if validation is unsuccessful show error
            if (!results.IsValid)
            {
                var errors = _exceptionInterface.DisplayErrors(results.Errors);
                return Ok(errors);
            }

            try
            {
                var user = await _userManager.GetUserAsync(User);
                user.Language = profileModelView.Language;

                await _userManager.UpdateAsync(user);

                return Ok(new ResponseMessage { IsSuccess = true, Message = $"your language setting have been updated" });
            }
            catch (Exception e)
            {
                Console.WriteLine($"A probleme while saving: '{e}'");

                return Ok(new ResponseMessage { IsSuccess = false, Message = $"A probleme while saving" });
            }
        }


        [HttpPost("setting/role/")]
        public async Task<IActionResult> AccountEditgRoleAsync([FromBody] ProfileModelView profileModelView)
        {
            // check validation
            AccountEditRoleValidator validator = new AccountEditRoleValidator();
            ValidationResult results = validator.Validate(profileModelView);

            // if validation is unsuccessful show error
            if (!results.IsValid)
            {
                var errors = _exceptionInterface.DisplayErrors(results.Errors);
                return Ok(errors);
            }

            try
            {
                var user = await _userManager.GetUserAsync(User);
                await _userManager.AddToRolesAsync(user, new[] { "Admin", "Esthetician" });

                return Ok(new ResponseMessage { IsSuccess = true, Message = $"your role have been updated" });
            }
            catch (Exception e)
            {
                Console.WriteLine($"A probleme while saving: '{e}'");

                return Ok(new ResponseMessage { IsSuccess = false, Message = $"A probleme while saving" });
            }
        }
    }
}
