const path = require('path')

module.exports = {
  src: path.resolve(__dirname, '../src'), // source files
  build: path.resolve(__dirname, '../../wwwroot'), // production build files
  tmpl: path.resolve(__dirname, '../../Views/Shared'), // production build files
  static: path.resolve(__dirname, '../public'), // static files to copy to build folder
}
