﻿import IMask from 'imask'

export const Mask = (id, mask) => {
    IMask(
        document.getElementById(id), {
        mask: mask
    })
}
