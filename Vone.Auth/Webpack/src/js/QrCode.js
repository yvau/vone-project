export function LoadQrCode(url) {
    new QRCode(document.getElementById('qrCode'), {
        text: url,
        width: 256,
        height: 256
    })
}